package controller;

import java.net.*;
import java.io.*;
import java.util.*;

/**
* 基于请求响应模型的客户端套接字，只有发送报文一个方法
* @author 林智凯
* @version 1.0
*/
public class UserClient
{
	/**
	   * 这个方法用于向服务器发送请求
	   * @param actionCode 用户的操作行为码，String
	   * @param username MD5形式的用户名，String
	   * @param password MD5形式的密码，actionCode为3时有多一个新密码，String
	   * @return 该请求是否成功，Boolean
	   */
   public static boolean sendRequest(String actionCode, String username, String password)
   {
      String serverName = "127.0.0.1";
      int port = 12000;
      try
      {
         //System.out.println("连接到主机：" + serverName + " ，端口号：" + port);
         Socket client = new Socket(serverName, port);
         //System.out.println("远程主机地址：" + client.getRemoteSocketAddress());
         OutputStream outToServer = client.getOutputStream();
         DataOutputStream out = new DataOutputStream(outToServer);
         
         //明文写入操作码，跟着用户名即密码
         String Plaintext = actionCode + " " + username + " " + password;
         //对明文进行 base64加密
         String ciphertext = Base64.getEncoder().encodeToString(Plaintext.getBytes("utf-8"));
         
         //向套接字传输密文
         out.writeUTF(ciphertext);
         InputStream inFromServer = client.getInputStream();
         DataInputStream in = new DataInputStream(inFromServer);
         //System.out.println("服务器响应： " + in.readUTF());
         
         //读取响应信息，base64解密后将bypes转为string
         String result_decode = new String(Base64.getDecoder().decode(in.readUTF()));
         if(result_decode.equals("true")) {
        	 client.close();
        	 return true;
         }
         else {
        	 client.close();
        	 return false;
         }
         
      }catch(IOException e)
      {
         e.printStackTrace();
      }
      
	return false;
   }
}