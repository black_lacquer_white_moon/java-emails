package model;

import java.sql.Timestamp;

/**
* Emails 类为邮件对象，存储一封邮件的基本信息，并附带有属性访问器和修改器
* @author 林智凯
* @version 1.0
*/
public class Message {
	
	private int id;    //邮件 id
	private String title;    //邮件标题
	private String user;    //收件用户
	private String addresser;    //发件人
	private String text;    //邮件正文
	private Timestamp time;    //邮件发送/接收时间
	
	public int getId() {
		return id;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getUser() {
		return user;
	}
	
	public String getAddresser() {
		return addresser;
	}
	
	public String getText() {
		return text;
	}
	
	public Timestamp getTime() {
		return time;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public void setUser(String user) {
		this.user = user;
	}
	
	public void setAddresser(String addresser) {
		this.addresser = addresser;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public void setTime(Timestamp time) {
		this.time = time;
	}
	
}
