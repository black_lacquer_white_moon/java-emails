package model;

import java.util.LinkedList;
import util.MD5Util;
import util.Transformation;

/**
* 这个类存储了用户名及其MD5加密，并且将调用UserBehavior类中的方法，实例化后绑定一台购物车给该用户
* @author 林智凯
* @version 1.1
*/
public class Customer {
	private final String username;
	private final String username_md5;
	private LinkedList<Message> Inbox;    //收件箱
	private LinkedList<Message> Outbox;    //发件箱
	
	/**
	   * 这个方法是customer对象的构造器
	   * @param username 用户名，String
	   * @return customer对象
	   */
	public Customer(String username) {
		this.username = username;
		this.username_md5 = MD5Util.getMD5Str(username);
	}
	
	/**
	   * 访问字段username，用户名
	   * @return username 用户名
	   */
	public String getUsername() {
		return username;
	}

	/**
	   * 访问字段username_md，加密后的用户名
	   * @return username_md 加密后的用户名
	   */
	public String getUsername_md5() {
		return username_md5;
	}

	public LinkedList<Message> getInbox() {
		return Inbox;
	}

	public LinkedList<Message> getOutbox() {
		return Outbox;
	}

	public void setInbox() {
		Inbox = Transformation.initializeInbox(this.username);
	}

	public void setOutbox() {
		Outbox = Transformation.initializeOutbox(this.username);
	}

}
