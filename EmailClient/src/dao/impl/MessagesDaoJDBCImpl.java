package dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import dao.MessagesDAO;
import model.*;
import util.MysqlConnect;

/**
* 基于 MySQL 实现的 emailsActionDAO 接口
* @author 林智凯
* @version 1.0
*/
public class MessagesDaoJDBCImpl implements MessagesDAO{
	
	/**
	   * 基于 MySql 数据库实现的 getSendedEmails() 方法
	   * @param username 用户名，String
	   * @return 已发送的邮件集 LinkedList<Messages>
	 * @throws SQLException 
	   */
	public static ResultSet getSendedEmails(String username) throws SQLException {
		Connection conn = null;    //创建 Connection 数据库连接对象
		Statement statement = null;    //创建静态 SQL 语句 Statement 对象
		ResultSet rs = null;	//创建 ResultSet 结果集对象
		
		try {
			conn = MysqlConnect.connectDatabase();    //数据库连接
			statement = conn.createStatement();    //初始化静态 SQL语句
			String sqlSelect = "SELECT * FROM emails WHERE binary addresser = '%s' ORDER BY id DESC;";
			rs = statement.executeQuery(String.format(sqlSelect, username));
			return rs;
			
		}catch (SQLException sqle) {
			throw sqle;
		}catch(Exception e){
			throw e;
		}/*finally{    //关闭所有资源
			MysqlConnect.close(rs);
			MysqlConnect.close(statement);
			MysqlConnect.close(conn);
		}*/
	}
	
	/**
	   * 基于 MySql 数据库实现的 getReceivedEmails() 方法
	   * @param username 用户名，String
	   * @return 已接收的邮件集 LinkedList<Emails>
	 * @throws SQLException 
	   */
	public static ResultSet getReceivedEmails(String username) throws SQLException {
		Connection conn = null;    //创建 Connection 数据库连接对象
		Statement statement = null;    //创建静态 SQL 语句 Statement 对象
		ResultSet rs = null;	//创建 ResultSet 结果集对象
		
		try {
			conn = MysqlConnect.connectDatabase();    //数据库连接
			statement = conn.createStatement();    //初始化静态 SQL语句
			String sqlSelect = "SELECT * FROM emails WHERE binary user = '%s' ORDER BY id DESC;";
			rs = statement.executeQuery(String.format(sqlSelect, username));
			return rs;
			
		}catch (SQLException sqle) {
			throw sqle;
		}catch(Exception e){
			throw e;
		}/*finally{    //关闭所有资源
			MysqlConnect.close(rs);
			MysqlConnect.close(statement);
			MysqlConnect.close(conn);
		}*/
	}
	
	/**
	   * 基于 MySql 数据库实现的 sendEmail() 方法
	   * @param a_email 要提交的邮件，Emails
	   * @return 操作是否成功，boolean
	 * @throws SQLException 
	   */
	public static boolean sendEmail(Message a_email) throws SQLException {
		Connection conn = null;    //创建 Connection 数据库连接对象
		Statement statement = null;    //创建静态 SQL 语句 Statement 对象
		boolean flag = false;
		//Timestamp time = new Timestamp(System.currentTimeMillis());    //获取当前时间
		
		try {
			conn = MysqlConnect.connectDatabase();    //数据库连接
			statement = conn.createStatement();    //初始化静态 SQL语句
			String sqlInsert = " INSERT INTO emails(title, user, addresser, text, sendtime) values('%s','%s','%s','%s','%s'); ";
			//判断插入是否成功
			if(statement.executeUpdate(String.format(sqlInsert, a_email.getTitle(), a_email.getUser(), a_email.getAddresser(), a_email.getText(), a_email.getTime())) != 0) {
				flag = true;
			}
			else {
				flag = false;
			}
			
		}catch (SQLException sqle) {
			throw sqle;
		}catch(Exception e){
			throw e;
		}finally{    //关闭所有资源
			MysqlConnect.close(statement);
			MysqlConnect.close(conn);
		}
		
		return flag;
	}
	
	/**
	   * 这个方法将基于select查找用户名是否已存在
	   * @param username 被查找的用户名
	   * @return true为用户名已存在，false为用户名不存在
	 * @throws SQLException 数据库异常
	   */
	public static boolean selectUsername(String username) throws SQLException {
		Connection conn = null;    //创建 Connection 数据库连接对象
		Statement statement = null;    //创建静态 SQL 语句 Statement 对象
		ResultSet rs = null;	//创建 ResultSet 结果集对象
		boolean flag = false;
		
		try {
			conn = MysqlConnect.connectDatabase();    //数据库连接
			statement = conn.createStatement();    //初始化静态 SQL语句
			String sqlSelect = "SELECT username FROM users WHERE binary username = '%s';";
			//查询用户名是否存在
			rs = statement.executeQuery(String.format(sqlSelect, username));
			rs.last();
			if(rs.getRow() > 0) {
				flag = true;    //用户名存在
			}
			else {
			    flag = false;    //用户名不存在
			}
			
		}catch (SQLException sqle) {
			throw sqle;
		}catch(Exception e){
			throw e;
		}finally{    //关闭所有资源
			MysqlConnect.close(rs);
			MysqlConnect.close(statement);
			MysqlConnect.close(conn);
		}
		
		return flag;
	}
	
}
