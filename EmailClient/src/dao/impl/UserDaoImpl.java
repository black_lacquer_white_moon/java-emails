package dao.impl;

import controller.UserClient;
import dao.UserDAO;
import util.MD5Util;

/**
 * 实现UserBehaviorDAO接口，支持针对用户的行为
 * 
 * @author 林智凯
 * @version 1.0
 */
public class UserDaoImpl implements UserDAO {

	/**
	 * 这个方法用于向服务器发送用户注册的请求，允许在未登录状态调用
	 * 
	 * @param username 明文形式的用户名，String
	 * @param password 密码，String
	 * @return 注册操作是否成功，Boolean
	 */
	public static boolean registerUser(String username, String password) {
		return UserClient.sendRequest("1", MD5Util.getMD5Str(username), MD5Util.getMD5Str(password));
	}

	/**
	 * 这个方法用于向服务器发送用户登录的请求，允许在未登录状态调用
	 * 
	 * @param username 明文形式的用户名，String
	 * @param password 密码，String
	 * @return 登录操作是否成功，Boolean
	 */
	public static boolean signIn(String username, String password) {
		return UserClient.sendRequest("2", MD5Util.getMD5Str(username), MD5Util.getMD5Str(password));
	}

	/**
	 * 这个方法用于向服务器发送用户改密码的请求，不允许在未登录状态调用
	 * 
	 * @param username     MD5形式的用户名，String
	 * @param password     原密码，String
	 * @param new_password 新密码，String
	 * @return 改密码操作是否成功，Boolean
	 */
	public static boolean changePassword(String username, String password, String new_password) {
		return UserClient.sendRequest("3", username,
				MD5Util.getMD5Str(password) + " " + MD5Util.getMD5Str(new_password));
	}

	/**
	 * 这个方法用于向服务器注销用户注册的请求，不允许在未登录状态调用
	 * 
	 * @param username MD5形式的用户名，String
	 * @param password 密码，String
	 * @return 注销操作是否成功，Boolean
	 */
	public static boolean cancelUser(String username, String password) {
		return UserClient.sendRequest("4", username, MD5Util.getMD5Str(password));
	}
}
