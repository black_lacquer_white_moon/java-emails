package dao;

/**
* UserBehaviorDAO 接口指定了针对用户的行为
* @author 林智凯
* @version 1.0
*/
public interface UserDAO {
	
	/**
	   * 这个方法将实现用户的注册操作
	   * @param username 用户名，String
	   * @param password 密码，String
	   * @return 操作是否成功，boolean
	   */
	public static boolean registerUser(String username, String password) {
		return false;
	}
	
	/**
	   * 这个方法将实现用户的改密码操作
	   * @param username 用户名，String
	   * @param password 密码，String
	   * @param new_password 新密码，String
	   * @return 操作是否成功，boolean
	   */
    public static boolean changePassword(String username, String password, String new_password) {
		return false;
	}
    
    /**
	   * 这个方法将实现用户的登录操作
	   * @param username 用户名，String
	   * @param password 密码，String
	   * @return 操作是否成功，boolean
	   */
    public static boolean signIn(String username, String password) {
		return false;
	}
    
    /**
	   * 这个方法将实现用户的销户操作
	   * @param username 用户名，String
	   * @param password 密码，String
	   * @return 操作是否成功，boolean
	   */
    public static boolean cancelUser(String username,String password) {
		return false;
	}
}
