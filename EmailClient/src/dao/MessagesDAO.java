package dao;

import java.sql.ResultSet;
import model.*;

/**
* emailsActionDAO 接口指定了用户获取邮件资源的行为
* @author 林智凯
* @version 1.0
*/
public interface MessagesDAO {
	
	/**
	   * 这个方法将以 ResultSet 的形式，从数据库获取已发送的邮件
	   * @param username 用户名，String
	   * @return 已发送的邮件集 LinkedList<Emails>
	   */
	public static ResultSet getSendedEmails(String username) {
		return null;
	}
	
	/**
	   * 这个方法将以  ResultSet 的形式，从数据库获取接收到的邮件
	   * @param username 用户名，String
	   * @return 已接收的邮件集 LinkedList<Emails>
	   */
	public static ResultSet getReceivedEmails(String username) {
		return null;	
	}
	
	/**
	   * 这个方法将一个 Emails 对象存储到数据库中
	   * @param a_email 要提交的邮件，Emails
	   * @return 操作是否成功，boolean
	   */
	public static boolean sendEmail(Message a_email) {
		return false;	
	}
	
}
