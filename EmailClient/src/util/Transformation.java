package util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

import dao.impl.MessagesDaoJDBCImpl;
import model.*;

/**
* 该类将调用MySqlEmailsAction类中的方法，并将返回的结果集转换为LinkedList的形式返回
* @author 林智凯
* @version 1.0
*/
public class Transformation {

	/**
	   * 该方法将调用 getReceivedEmails() 方法或得结果集，并转换为LinkedList<Emails>的形式返回
	   * @param username 用户名，String
	   * @return 已发送的邮件集 LinkedList<Emails>
	 * @throws SQLException 
	   */
	public static LinkedList<Message> initializeInbox(String username){
		try {
			ResultSet rs = MessagesDaoJDBCImpl.getReceivedEmails(username);    //接收数据库返回的结果集
			
			//将结果集的数据存到LinkedList
			LinkedList<Message> inbox = new LinkedList<Message>();
			while(rs.next())
	        {
				Message a_email = new Message();    //读取单行记录，转换为 Emails 对象
				a_email.setId(rs.getInt("id"));
				a_email.setTitle(rs.getString("title"));
				a_email.setUser(rs.getString("user"));
				a_email.setAddresser(rs.getString("addresser"));
				a_email.setText(rs.getString("text"));
				a_email.setTime(rs.getTimestamp("sendtime"));
				
				inbox.add(a_email);    //加入收件箱
	        }
			return inbox;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	   * 该方法将调用 getSendedEmails() 方法或得结果集，并转换为LinkedList<Emails>的形式返回
	   * @param username 用户名，String
	   * @return 已发送的邮件集 LinkedList<Emails>
	 * @throws SQLException 
	   */
	public static LinkedList<Message> initializeOutbox(String username){
		try {
			ResultSet rs = MessagesDaoJDBCImpl.getSendedEmails(username);    //接收数据库返回的结果集
			
			//将结果集的数据存到LinkedList
			LinkedList<Message> outbox = new LinkedList<Message>();
			while(rs.next())
	        {
				Message a_email = new Message();    //读取单行记录，转换为 Emails 对象
				a_email.setId(rs.getInt("id"));
				a_email.setTitle(rs.getString("title"));
				a_email.setUser(rs.getString("user"));
				a_email.setAddresser(rs.getString("addresser"));
				a_email.setText(rs.getString("text"));
				a_email.setTime(rs.getTimestamp("sendtime"));
				
				outbox.add(a_email);    //加入收件箱
	        }
			return outbox;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
}
