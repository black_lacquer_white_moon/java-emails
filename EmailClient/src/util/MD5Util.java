package util;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
* 这个类是工具类，对字符串进行MD5加密
* @author 林智凯
* @version 1.0
*/
public class MD5Util {
    
	/**
	   * 方法返回传入字符串的MD5加密字符串
	   * @param str 被加密的明文，String
	   * @return 明文经MD5加密后的密文，String
	   */
    public static String getMD5Str(String str) {
        byte[] digest = null;
        try {
            MessageDigest md5 = MessageDigest.getInstance("md5");
            digest  = md5.digest(str.getBytes("utf-8"));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        //16是表示转换为16进制数
        String md5Str = new BigInteger(1, digest).toString(16);
        return md5Str;
    }
    
    public static void main(String[] args) {
    	System.out.println(MD5Util.getMD5Str("张三"));
    	System.out.println(MD5Util.getMD5Str("123456"));
    }
}